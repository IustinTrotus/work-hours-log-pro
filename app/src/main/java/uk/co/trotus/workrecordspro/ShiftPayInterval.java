package uk.co.trotus.workrecordspro;

import org.joda.time.DateTime;

/**
 * Created by root on 15/03/16.
 */
public class ShiftPayInterval {

    // TODO: 20/03/16 change dates with period
    DateTime startTime;
    DateTime endTime;

    double hourlyWage;
    WagesType type = WagesType.Standard;

    public WagesType getType() {
        return type;
    }

    public void setType(WagesType type) {
        this.type = type;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }


    public DateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(DateTime endTime) {
        this.endTime = endTime;
    }

    public double getHourlyWage() {
        return hourlyWage;
    }

    public void setHourlyWage(double hourlyWage) {
        this.hourlyWage = hourlyWage;
    }
}
