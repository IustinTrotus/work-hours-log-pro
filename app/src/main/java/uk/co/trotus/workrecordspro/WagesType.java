package uk.co.trotus.workrecordspro;

/**
 * Created by root on 15/03/16.
 */
public enum WagesType {
    Standard,
    Holiday,
    Overtime,
    Overtime2
}
