package uk.co.trotus.workrecordspro;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

public class ActivityViewShifts extends BaseActivity {
    DatabaseHelper db = new DatabaseHelper(this);

    ListView listView;

    LinearLayout jobSelectLinearLayout;
    List<Job> jobs;
    List<Shift> shifts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_shifts);

        initialiseVariables();

        addJobsToCheckboxes(jobs);
        addCustomListAdapter(listView, shifts);
        ShowToast("on create", this);
    }


    void addJobsToCheckboxes(List<Job> jobs) {

//jobSelectLinearLayout.removeAllViews();

        if (jobs.size() == 0)
            return;


        for (int i = 0; i < jobs.size(); i++) {
            Job job = jobs.get(i);

            CheckBox jobCheckbox = (CheckBox) LayoutInflater.from(this).inflate(R.layout.job_checkbox_layout, null, false);

            jobCheckbox.setText(job.getName());
            jobCheckbox.setTag(R.id.jobID, job.getID());

            jobSelectLinearLayout.addView(jobCheckbox);
            if (jobs.size() == 1) {
                jobCheckbox.setSelected(true);
            }

        }
    }

    void initialiseVariables() {
        jobs = db.getAllJobs(true);

        listView = (ListView) findViewById(R.id.allShiftsList);


        jobSelectLinearLayout = (LinearLayout) findViewById(R.id.jobSelectLinearLayout);

        shifts = db.getAllShifts();
    }

    void addCustomListAdapter(ListView listView, List<Shift> shifts) {

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.listview_item_row, listView,
                false);

        listView.addHeaderView(header, null, false);


        // get data from the table by the ListAdapter
        AllShiftsListAdapter customAdapter = new AllShiftsListAdapter(this, R.layout.listview_item_row, shifts);

        listView.setAdapter(customAdapter);
    }

    public void jobCheckBoxOnClick(View view) {

        if (view.getTag(R.id.jobID) == null) {
            ShowToast("job tag not set", this);
        } else {
            ShowToast("Job tag = " + view.getTag(R.id.jobID), this);
        }
    }
}
