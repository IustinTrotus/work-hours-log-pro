package uk.co.trotus.workrecordspro;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;

public class ActivityEditJob extends BaseActivity {

    //region declare variables
    static Job job = new Job();
    static EditText jobNameEditText;
    static EditText newPayRateAmount;
    static EditText notesEditText;
    static CheckBox jobEnabledCheckBox;

    static Button SelectJobPayRateStartDateBtn;

    static ArrayList<PayRate> payRates;

    static ArrayList<Job_PayRate> job_PayRatesHistory;

    static DateTime job_PayRate_StartDate = new DateTime().withMillisOfDay(0);
    // Database Helper
    static DatabaseHelper db;
    private Job_PayRate job_payRate;

    //endregion


    void LoadAJob(int jobID) {
        this.job = db.getJob(jobID);
        int jobPayid = db.getJob_PayRateId(job.getID());
        if (jobPayid>0)
            job.payRate = db.getPayRate(jobPayid);
        DisplayJobFields(job);
        findViewById(R.id.jobDeleteBtn).setVisibility(View.VISIBLE);

        job_PayRatesHistory = db.getJob_PayRateHistory(job.getID());
        AddPayRatesHistoryToCustomList(job_PayRatesHistory, payRates);

    }

    void DisplayJobFields(Job job) {
        jobNameEditText.setText(job.getName());
        jobEnabledCheckBox.setChecked(job.getEnable());

        notesEditText.setText(job.getNotes());

    }

    public void SaveJob(View v) {
        SaveJob(job);
    }

    void SaveJob(Job job  ) {


        if (ValidateUserInputForJob() == false)
            return;

        String jobName = jobNameEditText.getText().toString();
        String notes = notesEditText.getText().toString();

        job.setName(jobName);

        job.setEnabled(jobEnabledCheckBox.isChecked());

        job.setNotes(notes);


        if (job.getID() > 0) {
            db.updateJob(job);
            ShowToast("The Job has been Updated!", getApplicationContext());
        } else {
            job.setID((int) db.createJob(job));
            ShowToast("The Job has been saved!", getApplicationContext());
        }


        GoBackToManageJobs();
    }

    boolean ValidateUserInputForJob() {

        if (jobNameEditText.getText().toString().isEmpty()) {
            ShowToast("The Name field is empty!", context);
            return false;
        }

        return true;
    }

    // TODO: 14/03/16 finish this function
    public void saveNewPayRate(View v) {
        PayRate payRate = new PayRate();

        if(newPayRateAmount.getText().toString().isEmpty()){
            ShowToast("Please type in the Hourly Pay Rate!", this);
            return;
        }
        try {

            payRate.setHourlyRate(Double.parseDouble(newPayRateAmount.getText().toString()));
        }catch (Error e){
            ShowToast("The Hourly Pay Rate must be a number \n\n\n" + e.getLocalizedMessage(), this);
            return;
        }

        Long newPayRateId = db.createPayRate(payRate);
        String selectedDate = SelectJobPayRateStartDateBtn.getText().toString();

        DateTimeFormatter formatter = DateTimeFormat.forPattern("E d MMM yyyy");
        DateTime dt = formatter.parseDateTime(selectedDate);

        Job_PayRate job_payRate = new Job_PayRate();

        job_payRate.setStartDate(dt.withMillisOfDay(0).getMillis());
        job_payRate.setJobID(job.getID());
        job_payRate.setPayRateID(newPayRateId);

        db.createJob_PayRate(job_payRate);

        HideNewPayAmountLayout();
        refreshPayRatesHistory();

    }

    public void DeleteJob(View v) {
        if (job.getID() > 0)
            db.deleteJOB(job.getID());
        // TODO: 15/08/2015 error handle job ID < 1

        GoBackToManageJobs();


    }

    void refreshPayRatesHistory() {
        LinearLayout payRatesHistoryLayout = (LinearLayout) findViewById(R.id.jobPayRatesHistoryLayout);

        payRatesHistoryLayout.removeAllViews();

        job_PayRatesHistory = db.getJob_PayRateHistory(job.getID());

        payRates = db.getAllPayRates(false);
        AddPayRatesHistoryToCustomList(job_PayRatesHistory, payRates);
    }


    void AddPayRatesHistoryToCustomList(ArrayList<Job_PayRate> payRatesHistory, ArrayList<PayRate> payRates) {

        LinearLayout payRatesHistoryLayout = (LinearLayout) findViewById(R.id.jobPayRatesHistoryLayout);

        for (Job_PayRate job_PayRate : payRatesHistory) {
            for (PayRate payRate : payRates) {
                if (payRate.getId() == job_PayRate.getPayRateID()) {

                    View payRateLayout = LayoutInflater.from(this).inflate(R.layout.payratelayout, null, false);

                    TextView amount = (TextView) payRateLayout.findViewById(R.id.payRateAmount);
                    TextView startDate = (TextView) payRateLayout.findViewById(R.id.payRateDateButton);

                    amount.setText((Double.toString(payRate.getHourlyRate())));

                    startDate.setText(MakeSimpleTextForDateButtons(new DateTime(job_PayRate.getStartDate())));

                    payRatesHistoryLayout.addView(payRateLayout);
                    break;
                }
            }

        }

    }


    //region Display / Hide Layouts for Adding new Pay Rates
    private void ReadPayRateForNewJobs() {
        ReadNewPay();
        findViewById(R.id.CancelNewPayBtn).setVisibility(View.GONE);
    }

    void ReadNewPay() {
        findViewById(R.id.PayRateStartLayout).setVisibility(View.VISIBLE);
        findViewById(R.id.newPayRateAmountLayout).setVisibility(View.VISIBLE);

        findViewById(R.id.ReadNewPayBtn).setVisibility(View.GONE);
        findViewById(R.id.CancelNewPayBtn).setVisibility(View.VISIBLE);
        findViewById(R.id.saveNewPayRateButton).setVisibility(View.VISIBLE);
    }

    public void ReadNewPay(View v) {
        ReadNewPay();
    }


    public void HideNewPaySelectLayout(View v) {

        HideNewPayAmountLayout();
    }

    private void HideNewPayAmountLayout() {
        findViewById(R.id.CancelNewPayBtn).setVisibility(View.GONE);
        findViewById(R.id.saveNewPayRateButton).setVisibility(View.GONE);

        newPayRateAmount.setText("");
        findViewById(R.id.newPayRateAmountLayout).setVisibility(View.GONE);
        findViewById(R.id.PayRateStartLayout).setVisibility(View.GONE);

        findViewById(R.id.ReadNewPayBtn).setVisibility(View.VISIBLE);
        SelectJobPayRateStartDateBtn.setText(MakeTextForDateButtons(new DateTime()));
    }

    //endregion

    public void Select_Job_PayRate_StartDate(View v) {

        if (v == (SelectJobPayRateStartDateBtn))
            showDatePickerDialog("job_PayRate_StartDate", job_PayRate_StartDate);
    }

    public void CatchReturnedDate(int year, int month, int day) {
        String tag = datePickerDialog.getTag();

        if (tag.equals("job_PayRate_StartDate")) {
            job_PayRate_StartDate = job_PayRate_StartDate.withDate(year, month + 1, day);
        }
        UpdateTextOnButtonsAndLabels();
    }

    private void UpdateTextOnButtonsAndLabels() {
        SelectJobPayRateStartDateBtn.setText(MakeTextForDateButtons(job_PayRate_StartDate));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_job);
        InitializeVariables();
        UpdateTextOnButtonsAndLabels();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();

            if (extras != null) {
                if (extras.getBoolean("loadASpecificJob") && (extras.getInt("jobIDToBeLoaded") > 0)) {
                    job.setID(extras.getInt("jobIDToBeLoaded"));
                    LoadAJob(job.getID());
                }
            } else if (job.getID() > 0)
                job = new Job();
        }

        if (job.getID() == 0)
            ReadPayRateForNewJobs();
    }

    void InitializeVariables() {
        context = getApplicationContext();
        jobNameEditText = (EditText) findViewById(R.id.jobNameEditText);
        notesEditText = (EditText) findViewById(R.id.notesEditText);
        jobEnabledCheckBox = (CheckBox) findViewById(R.id.jobEnabledCheckBox);
        newPayRateAmount = (EditText) findViewById(R.id.newPayRateAmount);
        SelectJobPayRateStartDateBtn = (Button) findViewById(R.id.SelectJobPayRateStartDateBtn);

        db = new DatabaseHelper(getApplicationContext());

        payRates = db.getAllPayRates(false);

        // TODO: 31/08/2015 analitycs no payrates available, error
    }

    void GoBackToManageJobs() {
        Intent intent = new Intent(this, ActivityManageJobs.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}