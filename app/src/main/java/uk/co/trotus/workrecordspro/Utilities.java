package uk.co.trotus.workrecordspro;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Iustin on 04/08/2015.
 */
public class Utilities {
    public void CreateDefaultJob(Context context) {
        DatabaseHelper db = new DatabaseHelper(context);
        Job job = new Job(0);
        job.setName("Default job");
        job.setEnabled(true);
        job.payRate.setHourlyRate(0);

        long jobID = db.createJob(job);
        long payRateID = db.createPayRate(job.payRate);
        db.createJob_PayRate(jobID, payRateID, 1000);

    }

    public Period CalculateDatePeriod(DateTime startDate, DateTime endDate) {

        if (startDate.isBefore(endDate.plusSeconds(1)))
            return new Period(startDate, endDate.plusSeconds(37), PeriodType.minutes());

        return new Period(startDate, endDate, PeriodType.minutes());
    }


    double CalculateWages(Shift shift, Job job, List<PayAmount_TimeStamp> pay_timestamps) {
        if (job.getID() < 1) {
            return 0;
        }

        int minutesInWork = shift.getMinutesInWork();
        double totalShiftWages;

        List<ShiftPayInterval> payIntervals = getShiftPayIntervals(shift,job);




        totalShiftWages = 0;

        if (pay_timestamps.size() == 1) {
            totalShiftWages += minutesInWork / (double) 60 * pay_timestamps.get(0).amount;


            Log.e("wedges", "Simple: " + minutesInWork + "  Amount:  " + pay_timestamps.get(0).amount + "  total w:  " + totalShiftWages);

        }


        int minutes;
        if (pay_timestamps.size() > 1) {

            //use list entries 1 .. n - 1
            DateTime startDate = shift.getStartDate();

            for (int i = 0; i < pay_timestamps.size() - 1; i++) {

                minutes = CalculateDatePeriod(startDate,
                        new DateTime().withMillis(pay_timestamps.get(i + 1).timestamp)).getMinutes();

                totalShiftWages += minutes / (double) 60 * pay_timestamps.get(i).amount;
                startDate = new DateTime().withMillis(pay_timestamps.get(i + 1).timestamp);

                Log.e("wedges", "  additional : " + i + "  min: " + minutes + "  Amount:  " + pay_timestamps.get(i + 1).amount + "  total w:  " + totalShiftWages);
            }


//            //now use the last entry
            minutes = CalculateDatePeriod(
                    startDate, shift.getEndDate()).getMinutes();

            totalShiftWages += minutes / (double) 60 * pay_timestamps.get(pay_timestamps.size() - 1).amount;

            Log.e("wedges", "Additional final: " + "  min: " + minutes + "  Amount:  " + pay_timestamps.get(pay_timestamps.size() - 1).amount + "  total w:  " + totalShiftWages);

        }


        Log.e("wedges", "Total: " + Double.toString(totalShiftWages) + "\n\n");
        return totalShiftWages;
    }

    List<ShiftPayInterval> getShiftPayIntervals(Shift shift, Job job) {
        List<ShiftPayInterval> shiftPayIntervals = new ArrayList<>();


        int minutesInWork = shift.getMinutesInWork();
        int overtime = 0, overtime2 = 0;

        List<ShiftPayInterval> payIntervals = new ArrayList<>();

        if (job.Overtime.minutesBeforeOvertime > 0 && job.Overtime.minutesBeforeOvertime < minutesInWork) {

            if (job.Overtime2.minutesBeforeOvertime > 0 && job.Overtime2.minutesBeforeOvertime < minutesInWork) {


                overtime2 = minutesInWork - job.Overtime2.minutesBeforeOvertime;

                ShiftPayInterval ov2Interval = new ShiftPayInterval();

                ov2Interval.setStartTime(
                        shift.getStartDate()
                                .plusMinutes(
                                        job.Overtime2.minutesBeforeOvertime)
                );

                ov2Interval.setEndTime(
                        ov2Interval.getStartTime()
                                .plusMinutes(overtime2)
                );
                payIntervals.add(ov2Interval);
            }
            overtime = (minutesInWork - job.Overtime.minutesBeforeOvertime) - overtime2;

            ShiftPayInterval ov1Interval = new ShiftPayInterval();

            ov1Interval.setStartTime(
                    shift.getStartDate()
                            .plusMinutes(
                                    job.Overtime2.minutesBeforeOvertime)
            );

            ov1Interval.setEndTime(
                    ov1Interval.getStartTime()
                            .plusMinutes(overtime2)
            );

            payIntervals.add(ov1Interval);
        }

        int standardMinutesInWork;
        standardMinutesInWork =minutesInWork - (overtime + overtime2);

        ShiftPayInterval standardInterval = new ShiftPayInterval();

        standardInterval.setStartTime(
                shift.getStartDate()        );

        standardInterval.setEndTime(
                standardInterval.getStartTime()
                        .plusMinutes(standardMinutesInWork)
        );
        payIntervals.add(standardInterval);

        return shiftPayIntervals;
    }


}
