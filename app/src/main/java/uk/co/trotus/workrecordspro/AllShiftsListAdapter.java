package uk.co.trotus.workrecordspro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by iustin on 19/03/16.
 */
public class AllShiftsListAdapter   extends ArrayAdapter<Shift> {

    public AllShiftsListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public AllShiftsListAdapter(Context context, int resource, List<Shift> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.listview_item_row, null);
        }

        Shift shift = getItem(position);

        if (shift != null) {
            TextView job = (TextView) v.findViewById(R.id.listRow_job);
            TextView dateHour = (TextView) v.findViewById(R.id.listRow_dateHour);
            TextView hours = (TextView) v.findViewById(R.id.listRow_hours);

            TextView wages = (TextView) v.findViewById(R.id.listRow_wages);
            TextView overtime1 = (TextView) v.findViewById(R.id.listRow_overtime1);
            TextView overtime2 = (TextView) v.findViewById(R.id.listRow_overtime2);

            TextView holidayPay = (TextView) v.findViewById(R.id.listRow_holidayPay);
            TextView tips = (TextView) v.findViewById(R.id.listRow_tips);
            TextView notes = (TextView) v.findViewById(R.id.listRow_notes);

            if (job != null) {
                job.setText(shift.getJobID()+"");
            }

            if (dateHour != null) {

                dateHour.setText(shift.getStartDate().toLocalDate()+"");
            }

            if (hours != null) {
                hours.setText(shift.getMinutesInWork()+"");
            }


            if (wages != null) {
                // wages.setText(shift.getID());
            }

            if (overtime1 != null) {

                 overtime1.setText(shift.getOvertime()+"");
            }

            if (overtime2 != null) {
                overtime2.setText(shift.getOvertime2()+"");
            }


            if (holidayPay != null) {
                holidayPay.setText(shift.getHolidayPay() + "");
            }

            if (tips != null) {

                tips.setText("£0.0");
            }

            if (notes != null) {
                notes.setText(shift.getNotes());
            }

        }

        return v;
    }

}